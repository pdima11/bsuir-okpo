import ast
import os
from collections import defaultdict
from graphviz import Digraph
from config import WEB_RESOURCES_PATH

KEY_NODES = (ast.Module, ast.For, ast.While, ast.If)
AUXILIARY_NODES = (ast.Assign, ast.AugAssign, ast.Expr)


class NodeVisitor(object):
    def __init__(self):
        self.result = ''
        self.node_id = 0

    def visit(self, node, level=0):
        code_node = CodeNode(self.node_id, node.__class__.__name__, level)
        self.node_id += 1
        if not self._is_key_node(node) and not self._is_auxiliary_node(node):
            return code_node
        self.result += f'{"-" * level + self._node_to_str(node)}\n'

        if self._is_auxiliary_node(node):
            return code_node
        for field, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        if field == 'orelse':
                            code_node.children.append(CodeNode(self.node_id, 'Else', level=level + 1))
                            self.node_id += 1
                            self.result += f'{"-" * level + "Else"}\n'
                        code_node.children.append(self.visit(item, level=level + 1))
            elif isinstance(value, ast.AST):
                self.visit(value, level=level + 1)

        return code_node

    def _is_key_node(self, node):
        return isinstance(node, KEY_NODES)

    def _is_auxiliary_node(self, node):
        return isinstance(node, AUXILIARY_NODES)

    def _node_to_str(self, node):
        if isinstance(node, ast.AST):
            return node.__class__.__name__
        else:
            return repr(node)


class Graph(object):
    def __init__(self):
        self.edges = defaultdict(set)

    def add_edge(self, src, target):
        self.edges[src].add(target)


class CodeNode(object):
    def __init__(self, _id, name, level):
        self.id = _id
        self.name = name
        self.level = level
        self.children = []

    def __repr__(self):
        return f'CodeNode[ID={self.id}, Name={self.name}, Level={self.level}, Children={self.children})'


class VisualizationGraph(object):
    def __init__(self, filename):
        self.graph = Graph()
        self.filename = filename
        self.vizgraph = Digraph('G', filename=self.filename, format='jpg')

    def build(self, code_node):
        self._build_graph(code_node)
        for src in self.graph.edges:
            # if 'Else' in src:
            #     continue
            targets = self.graph.edges[src]
            for target in targets:
                # if 'Else' not in target:
                self.vizgraph.edge(src, target)

    def render(self):
        #os.remove(self.filename)
        #os.remove(self.filename + '.jpg')
        self.vizgraph.render()

    def _build_graph(self, code_node, rec=False):
        if code_node.name == 'Module':
            children = code_node.children
            self._add_edges(code_node, children)
        if rec and code_node.name in ['For', 'While']:
            children = code_node.children
            self._add_edges(code_node, children)
            return str(children[0].id) + children[0].name
        if rec and code_node.name == 'If':
            children = code_node.children
            self.graph.add_edge(str(code_node.id) + code_node.name, self._build_graph(children[0]))
            for i in range(1, len(children)):
                self.graph.add_edge(str(children[i - 1].id) + children[i - 1].name, self._build_graph(children[i]))
                if children[i - 1].name in ['For', 'While', 'If']:
                    self.graph.add_edge(str(children[i - 1].id) + children[i - 1].name, self._build_graph(children[i - 1], rec=True))
                if children[i].name in ['For', 'While', 'If']:
                    self.graph.add_edge(str(children[i].id) + children[i].name, self._build_graph(children[i], rec=True))
            return str(children[0].id) + children[0].name
        return str(code_node.id) + code_node.name

    def _add_edges(self, parent, children):
        if len(children) == 1:
            self.graph.add_edge(str(parent.id) + parent.name, self._build_graph(children[0]))
            self._build_graph(children[0], rec=True)
        for i in range(1, len(children)):
            self.graph.add_edge(str(children[i - 1].id) + children[i - 1].name, self._build_graph(children[i]))
            if children[i - 1].name in ['For', 'While', 'If']:
                self.graph.add_edge(str(children[i - 1].id) + children[i - 1].name, self._build_graph(children[i - 1], rec=True))

    # def _build_graph(self, code_node, rec=False):
    #     if code_node.name == 'Module':
    #         children = code_node.children
    #         self._add_edges(children)
    #     if rec and code_node.name in ['For', 'While']:
    #         children = code_node.children
    #         self._add_edges(children)
    #         return str(children[0].id) + children[0].name
    #     if rec and code_node.name == 'If':
    #         children = code_node.children
    #         for i in range(1, len(children)):
    #             self.graph.edges[str(children[i - 1].id) + children[i - 1].name].append(self._build_graph(children[i]))
    #             if children[i - 1].name in ['For', 'While', 'If']:
    #                 self.graph.edges[str(children[i - 1].id) + children[i - 1].name].append(
    #                     self._build_graph(children[i - 1], rec=True))
    #             if children[i].name in ['For', 'While', 'If']:
    #                 self.graph.edges[str(children[i].id) + children[i].name].append(
    #                     self._build_graph(children[i], rec=True))
    #         return str(children[0].id) + children[0].name
    #     return str(code_node.id) + code_node.name
    #
    # def _add_edges(self, children):
    #     if len(children) == 1:
    #         self._build_graph(children[0], rec=True)
    #     for i in range(1, len(children)):
    #         self.graph.edges[str(children[i - 1].id) + children[i - 1].name].append(self._build_graph(children[i]))
    #         if children[i - 1].name in ['For', 'While', 'If']:
    #             self.graph.edges[str(children[i - 1].id) + children[i - 1].name].append(
    #                 self._build_graph(children[i - 1], rec=True))


class McCabe(object):
    def __init__(self):
        self.visitor = NodeVisitor()
        self.str_tree = None
        self.code_tree = None
        self.key_words = ['While', 'For', 'If', 'Module']

    def build_tree(self, code):
        tree = ast.parse(code)
        self.code_tree = self.visitor.visit(tree)
        self.str_tree = self.visitor.result.replace('-', '  ')
        return self.code_tree

    def calculate(self):
        return sum(1 for line in self.str_tree.splitlines() if any(key_word in line for key_word in self.key_words))



# if __name__ == '__main__':
#     code2='''
# while True:
#     print('Hello')
#     '''
#     mccabe = McCabe()
#     code_tree = mccabe.build_tree(code)
#     print(code_tree)
#     vg = VisualizationGraph()
#     vg.build(code_tree)
#     vg.render()



