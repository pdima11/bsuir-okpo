import os

APP_PORT = 8000
DATABASE_URL = os.environ.get(
    key='DATABASE_URL',
    default='postgres://udqgwzpzgjderx:873b7512df24f67f0e9025cccb37ed76e8475c82e4d6d59b191ba4802b0bda58@ec2-54-225-116-36.compute-1.amazonaws.com:5432/d7hj80jqeri2ia'
)

WEB_RESOURCES_PATH = os.path.join(os.getcwd(), 'static')