import os

from flask import Flask, request, url_for, render_template, redirect, send_from_directory, after_this_request
from http import HTTPStatus
from db import execute, SQL
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin, current_user
from config import APP_PORT
from werkzeug.utils import secure_filename
from mccabe import McCabe, VisualizationGraph

STUB_FILE_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'stubfile')
GRAPH_FILE_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'maccabe-graph')
UPLOAD_FOLDER = os.getcwd()
LAB2_CODE_EXAMPLE = '''x = 31
y = 0
z = 'ddd'
for i in range(11):
    print(i)
while x < 100:
    if x % 2 == 0:
        y = 0
    else:
        y = 1
    x += 1
print('End')
'''

app = Flask(__name__, static_url_path='/static')
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
app.config['SECRET_KEY'] = "secret key"
app.config['MAX_CONTENT_LENGTH'] = 21 * 1024 * 1024


class User(UserMixin):
    def __init__(self, _id):
        self.id = _id


@app.route('/', methods=['GET'])
def home():
    return render_template('home.html'), HTTPStatus.OK


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        username, password = request.form['username'], request.form['password']

        user_id = execute(SQL.get_user_by_name, [username])

        if user_id:
            return render_template('signup.html', error='A user with this username already exists')

        user_id = execute(SQL.create_user, [username, password])[0][0]
        size_limit = execute(SQL.get_size_limit_by_rate, ['Basic'])[0][0]
        execute(SQL.update_size_limit, {'user_id': user_id, 'size_limit': size_limit})
        return redirect(url_for('login'))
    return render_template('signup.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username, password = request.form['username'], request.form['password']
        login_query = 'SELECT user_id FROM users u WHERE u.username=\'' + username + '\' AND u.password=MD5(\'' + password + '\')'
        user_id = execute(login_query)
        if user_id:
            user = User(user_id[0][0])
            login_user(user)
            return redirect(url_for('lab1'))
        else:
            return render_template('login.html', error='The username or password is incorrect')
    return render_template('login.html')


@login_manager.user_loader
def load_user(user_id):
    return User(user_id)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/lab1', methods=['GET'])
def lab1():
    return render_template('lab1/lab1.html'), HTTPStatus.OK


@app.route('/lab1/pricing', methods=['GET'])
def lab1_pricing():
    _filter = request.args.get('filter')
    query = 'SELECT name, size_limit, price FROM rates'
    if _filter:
        query = query + 'WHERE price <=' + _filter
    rates = execute(query)

    return render_template('lab1/pricing.html', rates=enumerate(rates)), HTTPStatus.OK


@app.route('/lab1/apply-rate', methods=['POST'])
@login_required
def lab1_apply_rate():
    rate_name = request.form['rate-name']
    size_limit = execute(SQL.get_size_limit_by_rate, [rate_name])[0][0]
    execute(SQL.update_size_limit, {'user_id': current_user.id, 'size_limit': size_limit})
    return redirect(url_for('lab1_user_settings'))


@app.route('/lab1/add-file', methods=['GET', 'POST'])
@login_required
def lab1_add_file():
    if request.method == 'POST':
        files = request.files.getlist("files")
        user_size_limit = execute(SQL.get_size_limit_by_user, [current_user.id])[0][0]
        for i, file in enumerate(files):
            file_path = os.path.join(UPLOAD_FOLDER, secure_filename(file.filename))
            file.save(file_path)
            file_stat = os.stat(file_path)
            os.remove(file_path)

            active_user_size = execute(SQL.get_active_user_size, [current_user.id])[0][0]
            active_user_size = active_user_size if active_user_size else 0

            if active_user_size + file_stat.st_size > user_size_limit:
                error_files = list(map(lambda item: item.filename, files[i:]))

                return render_template('lab1/add-file.html', error=f'Unable to load next files: {error_files}. You exceeded size limit.')

            execute(SQL.add_file, [current_user.id, file.filename, file.content_type, file_stat.st_size])
        return redirect(url_for('lab1_user_files'))
    return render_template('lab1/add-file.html')


@app.route('/lab1/remove-file', methods=['POST'])
@login_required
def lab1_remove_file():
    file_id = request.form["file-id"]

    execute(SQL.remove_file_by_id, [file_id])
    return redirect(url_for('lab1_user_files'))


@app.route('/lab1/download-file', methods=['POST'])
@login_required
def lab1_download_file():
    file_id = request.form["file-id"]

    file = execute(SQL.get_file_by_id, [file_id])[0]
    filename, file_size = file[1], file[3]

    stub_path, stub_name = os.path.split(STUB_FILE_PATH)
    #stub_bytes = bytearray(os.urandom(file_size))
    file_path = os.path.join(stub_path, filename)

    #with open(STUB_FILE_PATH, 'wb') as downloaded_file:
    #    downloaded_file.write(stub_bytes)

    os.rename(STUB_FILE_PATH, file_path)

    @after_this_request
    def cleanup(response):
        #with open(file_path, 'wb') as stub_file:
        #    stub_file.write(bytearray([11, 71, 10]))
        os.rename(file_path, STUB_FILE_PATH)
        return response

    return send_from_directory(directory='static', filename=filename, as_attachment=True)


@app.route('/lab1/user-files', methods=['GET', 'POST'])
@login_required
def lab1_user_files():
    filter_query = ''
    if request.method == 'POST':
        filename = request.form['filename']

        if filename:
            filter_query = ' AND filename LIKE \'' + filename + '%%\''
    query = SQL.get_files_by_user + filter_query
    user_files = execute(query, [current_user.id])
    total_size = execute(SQL.get_active_user_size, [current_user.id])[0][0]
    total_size = total_size if total_size else 0

    return render_template('lab1/user-files.html', files=user_files, total_size=total_size), HTTPStatus.OK


@app.route('/lab1/user-settings', methods=['GET'])
@login_required
def lab1_user_settings():
    setting_names = execute(SQL.get_all_settings)
    setting_values = execute(SQL.get_settings_by_user, [current_user.id])
    setting_values = setting_values[0] if setting_values else []

    diff = len(setting_names) - len(setting_values)
    setting_names.extend([None] * diff)
    return render_template('lab1/user-settings.html', settings=zip(setting_names[1:], setting_values[1:])), HTTPStatus.OK


@app.route('/lab2', methods=['GET', 'POST'])
def lab2():
    if request.method == 'POST':
        src_code = request.form['source-code']
        mccabe = McCabe()
        if request.files['file']:
        #if request.files:
            file = request.files['file']
            code_tree = mccabe.build_tree(file.read())
        else:
            code_tree = mccabe.build_tree(src_code)
        vg = VisualizationGraph(GRAPH_FILE_PATH)
        vg.build(code_tree)
        vg.render()
        graph_img = os.path.basename(vg.filename) + '.jpg'
        return render_template('lab2.html', cyclomatic_complexity=mccabe.calculate(), graph_img=graph_img,
                               code_tree=mccabe.str_tree, code_example=src_code), HTTPStatus.OK
    return render_template('lab2.html', code_example=LAB2_CODE_EXAMPLE), HTTPStatus.OK


if __name__ == "__main__":
    app.run(port=APP_PORT, debug=False)
