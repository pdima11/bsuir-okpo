import psycopg2
import psycopg2.extras
from config import DATABASE_URL

_conn = None


class SQL(object):
    get_files_by_user = '''SELECT file_id, filename, content_type, size, uploaded_at
                        FROM files
                        WHERE user_id=%s'''

    get_file_by_id = '''SELECT file_id, filename, content_type, size, uploaded_at
                        FROM files
                        WHERE file_id=%s'''

    get_user_by_name = '''SELECT user_id
                        FROM users
                        WHERE username=%s'''

    get_settings_by_user = '''SELECT * 
                               FROM settings
                               WHERE user_id=%s'''

    get_all_settings = '''SELECT column_name
                           FROM information_schema.columns
                           WHERE table_name   = \'settings\''''

    get_size_limit_by_rate = '''SELECT size_limit 
                                FROM rates 
                                WHERE name=%s'''

    get_size_limit_by_user = '''SELECT size_limit 
                                    FROM settings 
                                    WHERE user_id=%s'''

    get_active_user_size = '''SELECT SUM(size) 
                                FROM files 
                                WHERE user_id=%s'''

    create_user = '''INSERT INTO users(username, password) 
                     VALUES(%s, MD5(%s))
                     RETURNING user_id'''

    add_file = '''INSERT INTO files(user_id, filename, content_type, size)
                        VALUES (%s, %s, %s, %s)'''

    update_size_limit = '''INSERT INTO settings(user_id, size_limit)
                        VALUES(%(user_id)s, %(size_limit)s)
                        ON CONFLICT(user_id) DO 
                        UPDATE
                        SET size_limit = %(size_limit)s;'''

    remove_file_by_id = '''DELETE
                    FROM files
                    WHERE file_id=%s'''


def get_conn():
    global _conn
    if _conn is None:
        _conn = psycopg2.connect(DATABASE_URL)

    return _conn


def execute(sql, params=None, *, autocommit=True):
    with get_conn().cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
        try:
            cur.execute(sql, params)
            if autocommit:
                get_conn().commit()

            if cur.description is not None:
                return list(cur)
        except psycopg2.DatabaseError as e:
            if autocommit:
                get_conn().rollback()
            raise e
