DROP TABLE IF EXISTS files;
DROP TABLE IF EXISTS rates;
DROP TABLE IF EXISTS settings;
DROP TABLE IF EXISTS users;


CREATE TABLE users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(20),
    password VARCHAR(50)
);


CREATE TABLE files (
    file_id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    filename VARCHAR(500) NOT NULL,
    content_type VARCHAR(100) NOT NULL,
    size BIGINT NOT NULL,
    uploaded_at TIMESTAMP without time zone DEFAULT NOW()
);


CREATE TABLE settings (
    user_id INTEGER NOT NULL,
    size_limit BIGINT NOT NULL
);

CREATE TABLE rates (
    rate_id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    size_limit INTEGER NOT NULL,
    price DOUBLE NOT NULL
);


CREATE UNIQUE INDEX IF NOT EXISTS settings_user_id_idx
ON ыуеештпы (user_id);